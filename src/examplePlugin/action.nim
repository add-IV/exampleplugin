import strutils, jsonschema, json, options

jsonSchema:
  Message:
    jsonrpc: string

  RequestMessage extends Message:
    id: int or string
    "method": string
    params?: any[] or any

  ResponseMessage extends Message:
    id: int or string or nil
    "result"?: any
    error?: ResponseError

  ResponseError:
    code: int
    message: string
    data: any

  NotificationMessage extends Message:
    "method": string
    params?: any[] or any

proc createMessage*(action: RequestMessage): string =
  let content = pretty action.JsonNode
  let length = content.len
  result = "Content-Length: " & $length & "\r\n\r\n" & content

proc getContent*(message: string): JsonNode=
  let
    split = message.splitLines(keepEol = true)
    length = parseInt split[0][16..<split[0].len-2]
    content = split[2..<split.len].join()
  assert length == content.len
  result.new()
  result = parseJson(content)

if isMainModule:
  let data = parseJson("""{"key": 3.14}""")
  let m = createMessage create(RequestMessage, "2.0", 2, "examplePlugin/stuff", none(JsonNode))
  echo m == "Content-Length: 68\c\n\c\n{\n  \"jsonrpc\": \"2.0\",\n  \"id\": 2,\n  \"method\": \"examplePlugin/stuff\"\n}"
