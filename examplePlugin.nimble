# Package

version       = "0.1.0"
author        = "add-iv"
description   = "an example of a plugin"
license       = "MIT"
srcDir        = "src"


# Dependencies

requires "nim >= 1.4.6", "jsonschema"
