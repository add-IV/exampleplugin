# This is just an example to get you started. You may wish to put all of your
# tests into a single file, or separate them into multiple `test1`, `test2`
# etc. files (better names are recommended, just make sure the name starts with
# the letter 't').
#
# To run these tests, simply execute `nimble test`.

import unittest, json

import examplePlugin
test "basic test":
  let message = "Content-Length: 68\c\n\c\n{\n  \"jsonrpc\": \"2.0\",\n  \"id\": 2,\n  \"method\": \"examplePlugin/stuff\"\n}"
  handleRequest("Content-Length: 68\c\n\c\n{\n  \"jsonrpc\": \"2.0\",\n  \"id\": 2,\n  \"method\": \"examplePlugin/stuff\"\n}")